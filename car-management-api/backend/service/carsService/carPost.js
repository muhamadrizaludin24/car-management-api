const { cars }      = require('../../models');
var multiparty      = require ('multiparty');
const randomstring  = require('randomstring');
const path          = require ('path');
const fs            = require('fs');

async function carPost(req, res) {
    console.log('req body', req.body);
    var form    = new multiparty.Form();
    form.parse(req, async function (err, fields, files) {
        var ext             = path.extname(files.image[0].originalFilename);
        var objExt          = ext.split(".")
        var filename        = randomstring.generate(6);
        var readerStream    = fs.createReadStream(files.image[0].path);

        var dest_file       = path.join(process.env.IMAGES_DIRECTORY, filename + "." + objExt[objExt.length - 1]);
        var writerStream    = fs.createWriteStream(dest_file);
        readerStream.pipe(writerStream);
        var carCreate = await cars.create({
                plate: fields.plate[0],
                manufacture: fields.manufacture[0],
                model: fields.model[0],
                image: filename + "." + objExt[objExt.length - 1],
                rent_per_day: fields.rent_per_day[0],
                capacity: fields.capacity[0],
                description: fields.description[0],
                transmision: fields.transmision[0],
                type:fields.type[0],
                type_driver:fields.type_driver[0],
                year: fields.year[0],
                options: fields.options[0],
                specs: fields.specs[0],
                available_at: fields.available_at[0],
                // createdAt: fields.createdAt[0],
                // updatedAt: fields.updatedAt[0]

            }).then(cars => {
                return cars;
        })
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(JSON.stringify({message: "Success", display_message:"Post data Success ", data: carCreate}));
        
    })
}

module.exports = carPost