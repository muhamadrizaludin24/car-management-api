const { cars }    = require('../../models');

async function carGet(req, res) {
    try {
        const getCar = await cars.findAll().then(function (result) {
            return result
        });
        console.log('getCar', getCar)
        if (getCar != null) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "Success", display_message:"List Cars Success ", data: getCar}));
        } else {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "filed", display_message:"No data from database", data:""}));
        }
    } catch (error) {
        console.log( error);
    } 
}

module.exports = carGet