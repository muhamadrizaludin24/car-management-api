const { cars }      = require('../../models');
async function carDetail(req, res) {
    console.log('params', req.params);
    try {
        const detailCar = await cars.findOne({where: {id:parseInt(req.params.id)}}).then(function(result) {
            return result
        });
        if (detailCar != null) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "Sucsess", display_message:"Get Detail of Car Sucsess", data:detailCar}));
        } else {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "filed", display_message:"No data from databse", data:""}));
        }
    } catch (error) {
        console.log('error', error);
    }
}
module.exports = carDetail;