const { cars }      = require('../../models');
async function carDelete(req, res) {
    console.log('req params', req.params);
    try {
        const deleteCar = await cars.destroy({where: {id: parseInt(req.params.id)}}).then(function (result) {
            return result;
        });
        if (deleteCar != null) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "Success", display_message:"Delete data car Sucsess", data:deleteCar}));
        } else {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "filed", display_message:"No Data from database", data:""}));
        }
    } catch (error) {
        console.log('error', error);
    }
}

module.exports = carDelete