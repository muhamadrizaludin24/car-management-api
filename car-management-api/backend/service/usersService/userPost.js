const { users }    = require('../../models');

async function userPost(req, res){
    console.log("req body", req.body);
    var userCreate = await users.create({
        username: req.body.username,
        password: req.body.password,
        level_user: req.body.level_user
    }).then(users => {
        return users;
    })
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:"Create User Success ", data: userCreate}));
}

module.exports = userPost