var { users } = require('../../models')
var authorization  = require('../../library/authorization');
async function login(req, res){
    console.log('req params', req.body);
    const getUser = await users.findOne({ where: { username: req.body.username }}).then(function (result) {
        return result
    });
    if(getUser != null){
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(JSON.stringify({message: "Login Success", display_message: getUser, data: authorization.generateJWT(req.body.username)}));
    }else{
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(JSON.stringify({message: "Failed", display_message:"Login Failed ", data: ""}));
    }
}

module.exports = login