const { users }    = require('../../models');

async function userPut(req, res){
    console.log("req body", req.body);
    var userUpdate = await users.create({
        username: req.body.username,
        password: req.body.password,
        level_user: req.body.level_user
    }).then(users => {
        return users;
    })
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:"Updated User Success ", data: userUpdate}));
}

module.exports = userPut