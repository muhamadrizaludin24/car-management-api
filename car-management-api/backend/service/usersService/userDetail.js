const { users }      = require('../../models');
async function userDetail(req, res) {
    console.log('params', req.params);
    try {
        const detailUser = await users.findOne({where: {id:parseInt(req.params.id)}}).then(function(result) {
            return result
        });
        if (detailUser != null) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "Sucsess", display_message:"Get Detail of User Sucsess", data:detailUser}));
        } else {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "filed", display_message:"No data from database", data:""}));
        }
        
    } catch (error) {
        console.log('error', error)
    }
    
}
module.exports = userDetail;