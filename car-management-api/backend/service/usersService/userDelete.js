const { users }      = require('../../models');
async function userDelete(req, res) {
    console.log('req params', req.params);
    try {
        const deleteUser = await users.destroy({where: {id: parseInt(req.params.id)}}).then(function (result) {
            return result;
        });
        if (deleteUser != null) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "Success", display_message:"Delete data  Sucsess", data:deleteUser}));
        } else {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "filed", display_message:"No Data from database", data:""}));
        }
    } catch (error) {
        console.log('error', error);
    }
}

module.exports = userDelete