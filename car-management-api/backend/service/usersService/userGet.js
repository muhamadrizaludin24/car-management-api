const { users }    = require('../../models');

async function userGet(req, res) {
    try {
        const getUser = await users.findAll().then(function (result) {
            return result
        });
        //console.log('getUser', getUser);
        if (getUser != null) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "Success", display_message:"List User Success ", data: getUser}));
        } else {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({message: "filed", display_message:"No data from database ", data: ""}));
        }
    } catch (error) {
        console.log('error', error);
    }
   
}

module.exports = userGet