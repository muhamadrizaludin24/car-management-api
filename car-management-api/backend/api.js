require('dotenv').config();
const express     = require('express');
var bodyParser    = require('body-parser')
const app         = express();
// parse application/json
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//user servis
// var index         = require('./service/index');
var login           = require('./service/usersService/userLogin');
var userGet         = require('./service/usersService/userGet');
var userPost        = require('./service/usersService/userPost');
var userPut        = require('./service/usersService/userPut');
var userDetail      = require('./service/usersService/userDetail');
var userDelete      = require('./service/usersService/userDelete');

// car servis
var carPost      = require('./service/carsService/carPost');
var carGet       = require('./service/carsService/carGet');
var carDetail    = require('./service/carsService/carDetail');
var carPut       = require('./service/carsService/carPut');
var carDelete    = require('./service/carsService/carDelete');
//var chekUser      = require('./middleware/checkUser');
var cors       = require('cors');
app.use(cors());
app.use(express.json());

// endpoint car
// app.get('/', index)
app.post('/carPost',carPost);
app.get('/car', carGet);
app.get('/carDetail/:id', carDetail);
app.put('/carPut', carPut);
app.delete('/carDelete/:id',carDelete);

//endpoint user
app.get('/login', login);
app.get('/user', userGet);
app.get('/userDetail/:id', userDetail);
app.post('/userPost', userPost);
app.put('/userPut', userPut);
app.delete('/userDelete/:id',userDelete);

app.listen(process.env.PORT_API, () => {
  console.log(`Example app listening on port ${process.env.PORT_API}`)
});