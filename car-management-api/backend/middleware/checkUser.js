var authorization  = require('../library/authorization');
var { users } = require('../models');
module.exports = async function checkUser(req, res, next) {
    let auth    = req.headers["authorization"];
    if(auth){
        try {
            //console.log("auth", auth);
            let token   = auth.slice(7);
            var tokenVerify = authorization.verifyJWT(token);
            const getUser = await users.findByPk("administrator","admin","member",{username: tokenVerify.username}).then(function (result) {
                return result;
            }); 
            //console.log("token", token);
            if(getUser != null){
                next();
            }else{
                res.setHeader("Content-Type", "application/json");
                res.writeHead(200);
                res.end(JSON.stringify({status: "204", message: "Failed", display_message: "User not Found", data: ""}));
            }
            
        } catch (err) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify({status: "204", message: "Failed", display_message: err, data: ""}));
            
        }
        
        
    }else{
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(JSON.stringify({status: "204", message: "Failed", display_message:"Token Required ", data: ""}));
    }
  };